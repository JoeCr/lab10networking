#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <readline/history.h>
#include <readline/readline.h>

void menu();
char get_choice();
void download_file(int s);
void list_files(int s);
void close_connection(int s);
int open_connection();

int main()
{
    int sockfd = open_connection();
    while(1)
    {
        menu();
        char c = get_choice();
        switch (c)
        {
            case 'l':
            case 'L':
                list_files(sockfd);
                break;
            case 'd':
            case 'D':
                download_file(sockfd);
                break;
            case 'q':
            case 'Q':
                close_connection(sockfd);
                exit(1);
            default:
                printf("Please enter d, l, or q\n");
                break;
        }
    }
}

void menu()
{
    printf("L)ist files\n");
    printf("D)ownload a file\n");
    printf("Q)uit\n");
    printf("\n");
}

char get_choice()
{
    char *line = readline("Choice: ");
    char first = line[0];
    free(line);
    return first;
}

int open_connection()
{
    struct sockaddr_in sa;
    int sockfd;
    
    struct hostent *he = gethostbyname("runwire.com");
    char *ip = he -> h_addr_list[0];
    
    sa.sin_family=AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr= *((struct in_addr *) ip);
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockfd == -1)
    {
        fprintf(stderr, "Can't connect.\n");
        exit(3);
    }
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1)
    {
        fprintf(stderr, "Can't connect.\n");
        exit(2);
    }
    char buf[1000];
    recv(sockfd, buf, 1000, 0);
    printf("%s", buf);
    return sockfd;
}

void list_files(int s)
{
    // Send LIST command
    char list[6];
    sprintf(list, "LIST\n");
    send(s, list, strlen(list), 0);
    char buf[1000];
    recv(s,buf,1000,0);
    printf("%s\n",buf);
    
    // Receive listing
    
    
    // Display to user
    
}

void download_file(int s)
{
    // Ask user which file
    char *line = readline("Name a File: ");
    
    // Find out how big the file is
    char getCommand[100];
    sprintf(getCommand,"SIZE %s\n",line);
    free(line);
    send(s,getCommand,sizeof(getCommand),0);
    char buf[1000];
    recv(s,buf,1000,0);
    int size;
    char garb[1000];
    if(buf[0]=='-')
    {
        printf("You must enter a valid file name.\n");
    }
    else
    {
        printf("%.4s\n",buf);
        sscanf(buf,"%s %d",garb,&size);
        printf("%d",size);
    }
    
    
    // Send GET command
    //sprintf(getCommand,"GET %s\n",line);
    //send(s,getCommand, sizeof(getCommand),0);
    
    // Receive data
    //FILE *h = fopen(line, "w");
    
    
    
    
    // Display to user
    
}

void close_connection(int s)
{
    // send QUIT
    char quit[10];
    sprintf(quit, "QUIT\n");
    send(s, quit, strlen(quit), 0);
    char buf[10];
    recv(s, buf, 10, 0);
    printf("%s", buf);
    close(s);
    
    // Close socket
}
